import Home from './views/Home.vue'
import Paid from './views/Paid.vue'
import NotFound from './views/NotFound.vue'

/** @type {import('vue-router').RouterOptions['routes']} */
export const routes = [
  { path: '/', component: Home, meta: { title: 'Home' } },
  {
    path: '/paid',
    meta: { title: 'Payment completed', transition: "fade" },
    component: Paid,
  },
  { path: '/:path(.*)', component: NotFound },
]
