# Test task

Task: [full description]( https://www.notion.so/itclub/Frontend-task-IT-Club-65f418b3d3d54f6d8dc9fad08d6bbab3)

## Description

Based on [vite-vue3-tailwind-starter](https://github.com/web2033/vite-vue3-tailwind-starter)
Vite template

All data are stored only in browser local storage, it's NOT a proper decision for real working
projects. Selecting payment method needs to be done in the future. Tried to add some animations
(two types: with components and with routes). Didn't restrict fullscreen usage, 'cause couldn't
properly test it on my device.
Pay button does nothing for now.

## Local usage

### Installation

```sh
yarn install
```

### Running

```sh
yarn run build
yarn run serve
```
or (dev mode)
```sh
yarn run dev
```


